import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class MagicZoo {

    final static String location = "Jeju Island";
    final static String ceo = "Mr.Jelly";
    final static int number = 000-000-000;


    public static void main(String[] args) throws InterruptedException {

        System.out.println("Welcome to magic Zoo! ");

        //AAnimals리스트에 동물 객체들을 넣어준다.
        Scanner sc = new Scanner(System.in);
        ArrayList<AAnimals> zooAnimals = new ArrayList<AAnimals>();
        zooAnimals.add(new Zebra());
        zooAnimals.add(new Elephant());
        zooAnimals.add(new Snake());
        zooAnimals.add(new Panda());
        zooAnimals.add(new Lion());
        zooAnimals.add(new Tiger());
        zooAnimals.add(new Kangaroo());




        //첫인사 및 메뉴 선택 요청
        System.out.println(" click the menu and press enter ! ");
        TimeUnit.SECONDS.sleep(1);
        System.out.print("1.animal ");
        TimeUnit.SECONDS.sleep(1);
        System.out.print("2.location ");
        TimeUnit.SECONDS.sleep(1);
        System.out.print("3.our number ");
        TimeUnit.SECONDS.sleep(1);
        System.out.print("4.ceo ");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("5.this month's animal ");

        //사용자가 선택한 메뉴의 답으로 이동
        int click = sc.nextInt();
        switch(click){
            case 1 :
                System.out.println("lets check out our animals !");
                sc.nextLine();
                for(int i=0;i<zooAnimals.size();i++){
                        System.out.println(zooAnimals.get(i).getAnimalName());
                }
                break;
            case 2 :
                System.out.println(location);
                break;
            case 3 :
                System.out.println(number);
                break;
            case 4 :
                System.out.println(ceo);
                break;
            case 5 :
                System.out.println("this month's animal is panda");
                break;
            default :
                System.out.println("press the number again ~ ");
                break;
        }

    }
}

//AAnimals를 가지고 있는 동물 객체들
abstract class AAnimals{
    String animalName;
    public String getAnimalName(){
        return this.animalName;
    }
    public void setAnimalName(String animalName){
        this.animalName = animalName;
    }
}

class Zebra extends AAnimals {
    public Zebra() {
        this.animalName = "zebra";
    }
}

class Snake extends AAnimals {
    public Snake() {
        this.animalName = "snake";
    }
}

class Elephant extends AAnimals {
    public Elephant() {
        this.animalName = "elephant";
    }
}

class Panda extends AAnimals{
    public Panda() {
        this.animalName = "panda";
    }
}

class Lion extends AAnimals{
    public Lion() {
        this.animalName = "lion";
    }
}
class Tiger extends AAnimals{
    public Tiger() {
        this.animalName = "tiger";
    }

}
class Kangaroo extends AAnimals{
    public Kangaroo() {this.animalName = "kangaroo";}
}
